package com.highscores.api.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.highscores.api.Application;
import com.highscores.api.entity.AbstractEntity;
import com.highscores.api.entity.Player;
import com.highscores.api.util.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class PlayerServiceTest {

    @Autowired
    private PlayerService playerService;


    @Test
    public void readAllTest() {
        List<AbstractEntity> players = playerService.readAll();
        assertEquals(players.size(), 11);
    }
    @Test
    public void findByTest() {
        Player player = (Player)playerService.findBy(10);
        assertEquals(player.getNickName(), "theBest");
    }

    @Test
    public void createTest() {
        Player player = new Player(120L,"John Wick",4,"wick",800L,40L,98L,9L,66L,1081L);
        Player playerTest = (Player)playerService.create(player);
        assertEquals(playerTest.getNickName(), "wick");
    }

    @Test
    public void updateTest() throws IOException {
        Player player = (Player)playerService.findBy(1);
        player.setNickName("bipBipRichie");
        Player playerTest = (Player)playerService.update(player);
        assertEquals(playerTest.getNickName(), "bipBipRichie");
    }

    @Test
    public void delete() {
        assertEquals(playerService.delete(9), true);
    }

    @Test
    public void getTop10OrderByOverallDescTest(){
        List<OverallResponse> players = playerService.getTop10OrderByOverallDesc();
        assertEquals(players.get(0).getNickName(),"theBest");
        assertEquals(players.size(),10);
    }

    @Test
    public void getTop10OrderByAttackDescTest(){
        List<AttackResponse> players = playerService.getTop10OrderByAttackDesc();
        assertEquals(players.get(0).getNickName(),"theBest");
        assertEquals(players.size(),10);
    }

    @Test
    public void getTop10OrderByDefenseDescTest(){
        List<DefenseResponse> players = playerService.getTop10OrderByDefenseDesc();
        assertEquals(players.get(0).getNickName(),"LannAstuto");
        assertEquals(players.size(),10);
    }

    @Test
    public void getTop10OrderByMagicDescTest(){
        List<MagicResponse> players = playerService.getTop10OrderByMagicDesc();
        assertEquals(players.get(0).getNickName(),"colorada");
        assertEquals(players.size(),10);
    }

    @Test
    public void getTop10OrderByCookingDescTest(){
        List<CookingResponse> players = playerService.getTop10OrderByCookingDesc();
        assertEquals(players.get(0).getNickName(),"Melkor");
        assertEquals(players.size(),10);
    }

    @Test
    public void getTop10OrderByCraftingDescTest(){
        List<CraftingResponse> players = playerService.getTop10OrderByCraftingDesc();
        assertEquals(players.get(0).getNickName(),"IT");
        assertEquals(players.size(),10);
    }

    @Test
    public void findByNameTest(){
        Player player = (Player)playerService.findByName("theBest");
        assertEquals(player.getNickName(),"theBest");
    }

}
