package com.highscores.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.highscores.api.Application;
import com.highscores.api.entity.Player;
import com.highscores.api.service.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;




@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class PlayerControllerTest {

    private MockMvc mvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    

    @Before
    public void configuration() throws Exception{
        this.mvc = webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void readAllTest() throws Exception{
        mvc.perform(get("/players")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].nickName", is("bipBip")));
    }

    @Test
    public void findByIdTest() throws Exception{
        mvc.perform(get("/players/1")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.name", is("Richie Tozier")));
    }

    @Test
    public void saveTest() throws Exception{
        Player playerTest = new Player();
        playerTest.setName("Test");
        playerTest.setNickName("test");
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(playerTest);

        mvc.perform(post("/players")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated());
    }

    @Test
    public void updateTest() throws Exception{
        String jsonInString = "{\"id\":1,\"name\":\"Richie Tozier\",\"level\":1,\"nickName\":\"bipBipRichie\",\"attack\":50,\"defense\":50,\"magic\":50,\"cooking\":50,\"crafting\":50,\"overall\":250,\"created_date\":\"2018-08-16T07:19:57\",\"updated_date\":\"2018-08-17T10:15:37\"}";
        System.out.println(jsonInString);

        mvc.perform(put("/players")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTest() throws Exception{
        mvc.perform(delete("/players/6")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    public void readAllByOverallTest() throws Exception{
        mvc.perform(get("/players/scores-overall")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].nickName", is("theBest")));
    }

    @Test
    public void readAllByAttackTest() throws Exception{
        mvc.perform(get("/players/scores-attack")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].nickName", is("theBest")));
    }

    @Test
    public void readAllByDefenseTest() throws Exception{
        mvc.perform(get("/players/scores-defense")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].nickName", is("LannAstuto")));
    }

    @Test
    public void readAllByMagicTest() throws Exception{
        mvc.perform(get("/players/scores-magic")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].nickName", is("colorada")));
    }

    @Test
    public void readAllByCookingTest() throws Exception{
        mvc.perform(get("/players/scores-cooking")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].nickName", is("Melkor")));
    }

    @Test
    public void readAllByCraftingTest() throws Exception{
        mvc.perform(get("/players/scores-crafting")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].nickName", is("IT")));
    }

    @Test
    public void findByNameTest() throws Exception{
        mvc.perform(get("/players?nickName=theBest")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.name", is("Glorfindel")));
    }

    @Test
    public void compareTest() throws Exception{
        mvc.perform(get("/players?player1=theBest&player2=IT")
                .contentType(APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isFound())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].nickName", is("theBest")))
                .andExpect(jsonPath("$[1].nickName", is("IT")));
    }




}
