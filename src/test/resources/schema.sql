DROP TABLE IF EXISTS players;
CREATE TABLE players (
 id bigint(20) NOT NULL AUTO_INCREMENT,
 created_date datetime NOT NULL,
 updated_date datetime DEFAULT NULL,
 attack bigint(20) NOT NULL,
 cooking bigint(20) NOT NULL,
 crafting bigint(20) NOT NULL,
 defense bigint(20) NOT NULL,
 level int(11) NOT NULL,
 magic bigint(20) NOT NULL,
 name varchar(255) NOT NULL,
 nick_name varchar(255) NOT NULL,
 overall bigint(20) NOT NULL,
 PRIMARY KEY (id)
)