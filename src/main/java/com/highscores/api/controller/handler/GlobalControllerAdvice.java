
package com.highscores.api.controller.handler;


import com.highscores.api.exception.ElementNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.bind.annotation.*;
import com.highscores.api.util.CustomResponse;
import org.springframework.http.ResponseEntity;

import javax.persistence.EntityNotFoundException;


@RestControllerAdvice
public class GlobalControllerAdvice {
    private static final Logger logger = LoggerFactory.getLogger(GlobalControllerAdvice.class);

    @ExceptionHandler({EntityNotFoundException.class, ResourceNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected CustomResponse handleresourceNotFoundException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse("Not found!",ex);
    }

    @ExceptionHandler({IllegalArgumentException.class, HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CustomResponse handleBadRequestException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse("Bad request!",ex);
    }

    @ExceptionHandler(HttpMediaTypeException.class)
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    protected CustomResponse handleUnsupportedMediaTypeException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse("Unsupported media type!",ex);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected CustomResponse handleInternalServerException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse("Internal server error!",ex);
    }

    private CustomResponse generateResponse(String name, Exception ex){
        CustomResponse response = new CustomResponse();
        response.setName(name);
        response.setMessage(ex.getMessage());
        return response;
    }


}
