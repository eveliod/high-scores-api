
package com.highscores.api.controller;

import com.highscores.api.entity.AbstractEntity;
import com.highscores.api.entity.Player;
import com.highscores.api.service.PlayerService;
import com.highscores.api.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/players")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    private Long zero = Long.valueOf(0);

    @GetMapping
    public ResponseEntity<?> readAll(){
        List<AbstractEntity> allPlayers =  playerService.readAll();
        return new ResponseEntity<>(allPlayers, HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        AbstractEntity player =  playerService.findBy(id);
        if (player!=null)
            return new ResponseEntity<>(player, HttpStatus.FOUND);
        else{
            CustomResponse response = new CustomResponse("Player not found.",id.toString());
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Player player){
        if (player == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else{
            player.setAttack(zero);
            player.setDefense(zero);
            player.setLevel(0);
            player.setMagic(zero);
            player.setCooking(zero);
            player.setCrafting(zero);
            player.setOverall();
            playerService.create(player);
            return new ResponseEntity<>(player, HttpStatus.CREATED);
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Player player){
        if (player == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else{
            player.setOverall();
            playerService.update(player);
            return new ResponseEntity<>(player, HttpStatus.OK);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        boolean result = playerService.delete(id);
        if(!result){
            CustomResponse response = new CustomResponse("Player not found.",id.toString());
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
        else{
            CustomResponse response = new CustomResponse("Deleted successfully!",id.toString());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }

    @GetMapping("/scores-overall")
    public ResponseEntity<?> readAllByOverall(){
        List<OverallResponse> allPlayers =  playerService.getTop10OrderByOverallDesc();
        return new ResponseEntity<>(allPlayers, HttpStatus.OK);
    }

    @GetMapping("/scores-attack")
    public ResponseEntity<?> readAllByAttack(){
        List<AttackResponse> allPlayers =  playerService.getTop10OrderByAttackDesc();
        return new ResponseEntity<>(allPlayers, HttpStatus.OK);
    }

    @GetMapping("/scores-defense")
    public ResponseEntity<?> readAllByDefense(){
        List<DefenseResponse> allPlayers =  playerService.getTop10OrderByDefenseDesc();
        return new ResponseEntity<>(allPlayers, HttpStatus.OK);
    }

    @GetMapping("/scores-magic")
    public ResponseEntity<?> readAllByMagic(){
        List<MagicResponse> allPlayers =  playerService.getTop10OrderByMagicDesc();
        return new ResponseEntity<>(allPlayers, HttpStatus.OK);
    }

    @GetMapping("/scores-cooking")
    public ResponseEntity<?> readAllByCooking(){
        List<CookingResponse> allPlayers =  playerService.getTop10OrderByCookingDesc();
        return new ResponseEntity<>(allPlayers, HttpStatus.OK);
    }

    @GetMapping("/scores-crafting")
    public ResponseEntity<?> readAllByCrafting(){
        List<CraftingResponse> allPlayers =  playerService.getTop10OrderByCraftingDesc();
        return new ResponseEntity<>(allPlayers, HttpStatus.OK);
    }

    @GetMapping(params = "nickName")
    public ResponseEntity<?> findByName(String nickName){
        AbstractEntity player =  playerService.findByName(nickName);
        if (player!=null)
            return new ResponseEntity<>(player, HttpStatus.FOUND);
        else{
            CustomResponse response = new CustomResponse("Player not found.",nickName);
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(params = {"player1","player2"})
    public ResponseEntity<?> compare(String player1, String player2){
        AbstractEntity firstPlayer =  playerService.findByName(player1);
        AbstractEntity secondPlayer =  playerService.findByName(player2);
        if (firstPlayer!=null && secondPlayer!=null)
            return new ResponseEntity<>(new ArrayList<>(Arrays.asList(firstPlayer,secondPlayer)), HttpStatus.FOUND);
        else{
            CustomResponse response = new CustomResponse("Players not found.",player1+" or "+player2+" not found.");
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }







}
