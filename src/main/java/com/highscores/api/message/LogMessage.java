
package com.highscores.api.message;

public enum LogMessage {

    READ_ALL("'Read Objects'"),
    FIND_OBJECT("'Find Object'"),
    CREATE_OBJECT("'Object Created'"),
    DELETE_OBJECT("'Object Deleted'"),
    UPDATE_OBJECT("'Object Updated'");

    private final String message;

    LogMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

