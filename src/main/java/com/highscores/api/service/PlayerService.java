
package com.highscores.api.service;

import com.highscores.api.entity.AbstractEntity;
import com.highscores.api.repository.PlayerRepository;
import com.highscores.api.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerService extends AbstractService{

    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository repository){
        super(repository);
        this.playerRepository = repository;
    }

    public List<OverallResponse> getTop10OrderByOverallDesc(){
        return playerRepository.findTop10ByOrderByOverallDesc();
    }

    public List<AttackResponse> getTop10OrderByAttackDesc(){
        return playerRepository.findTop10ByOrderByAttackDesc();
    }

    public List<DefenseResponse> getTop10OrderByDefenseDesc(){
        return playerRepository.findTop10ByOrderByDefenseDesc();
    }

    public List<MagicResponse> getTop10OrderByMagicDesc(){
        return playerRepository.findTop10ByOrderByMagicDesc();
    }

    public List<CookingResponse> getTop10OrderByCookingDesc(){
        return playerRepository.findTop10ByOrderByCookingDesc();
    }

    public List<CraftingResponse> getTop10OrderByCraftingDesc(){
        return playerRepository.findTop10ByOrderByCraftingDesc();
    }

    public AbstractEntity findByName(String name) {
        return playerRepository.findByNickName(name);
    }
}
