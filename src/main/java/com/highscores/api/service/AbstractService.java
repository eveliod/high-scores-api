package com.highscores.api.service;

import com.highscores.api.entity.AbstractEntity;
import com.highscores.api.message.LogMessage;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.slf4j.Logger;
import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;


public class AbstractService implements IService{
    private final JpaRepository repository;
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public AbstractService(JpaRepository repository){
        this.repository = repository;
    }

    @Override
    public AbstractEntity create(AbstractEntity entity){
        AbstractEntity saved = null;
        entity.setCreatedDate(LocalDateTime.now());
        entity.setUpdatedDate(LocalDateTime.now());
        saved = (AbstractEntity)repository.save(entity);
        logger.info(LogMessage.CREATE_OBJECT.getMessage());
        return saved;
    }

    @Override
    public AbstractEntity update(AbstractEntity entity){
        AbstractEntity saved = null;
        entity.setUpdatedDate(LocalDateTime.now());
        saved = (AbstractEntity)repository.save(entity);
        logger.info(LogMessage.UPDATE_OBJECT.getMessage());
        return saved;
    }

    @Override
    public boolean delete(long id) throws EntityNotFoundException {
        boolean result = true;

        if (repository.exists(id)){
            repository.delete(id);
            logger.info(LogMessage.DELETE_OBJECT.getMessage());
        }
        else {
            throw new EntityNotFoundException(Long.toString(id));
        }
        return result;
    }

    @Override
    public AbstractEntity findBy(long id) throws EntityNotFoundException {
        logger.info(LogMessage.FIND_OBJECT.getMessage());
        return (AbstractEntity) repository.findOne(id);
    }

    @Override
    public List<AbstractEntity> readAll() {
        return repository.findAll();
    }

}
