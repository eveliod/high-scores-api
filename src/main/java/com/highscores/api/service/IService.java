
package com.highscores.api.service;

import com.highscores.api.entity.AbstractEntity;
import java.util.List;
import javax.persistence.EntityNotFoundException;

public interface IService {
    public AbstractEntity create(AbstractEntity entity);

    public AbstractEntity update(AbstractEntity entity);

    public boolean delete(long id) throws EntityNotFoundException;

    public AbstractEntity findBy(long id) throws EntityNotFoundException;

    public List readAll();

    
}
