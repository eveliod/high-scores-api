
package com.highscores.api.repository;

import com.highscores.api.entity.AbstractEntity;
import com.highscores.api.entity.Player;
import com.highscores.api.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long>{
    public List<OverallResponse> findTop10ByOrderByOverallDesc();
    public List<AttackResponse> findTop10ByOrderByAttackDesc();
    public List<DefenseResponse> findTop10ByOrderByDefenseDesc();
    public List<MagicResponse> findTop10ByOrderByMagicDesc();
    public List<CookingResponse> findTop10ByOrderByCookingDesc();
    public List<CraftingResponse> findTop10ByOrderByCraftingDesc();
    public AbstractEntity findByNickName(String name);

}
