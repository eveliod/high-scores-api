package com.highscores.api.exception;

public class ElementNotFoundException extends Exception {
    private Long resourceId;

    public ElementNotFoundException(Long resourceId, String message) {
        super(message);
        this.resourceId = resourceId;
    }
}
