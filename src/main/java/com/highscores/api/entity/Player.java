package com.highscores.api.entity;

import lombok.*;

import javax.persistence.*;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "players")
@EqualsAndHashCode(callSuper = true)
public class Player extends AbstractEntity{

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "level", nullable = false)
    private int level;

    @Column(name = "nickName", nullable = false)
    private String nickName;

    @Column(name = "attack", nullable = false)
    private Long attack;

    @Column(name = "defense", nullable = false)
    private Long defense;

    @Column(name = "magic", nullable = false)
    private Long magic;

    @Column(name = "cooking", nullable = false)
    private Long cooking;

    @Column(name = "crafting", nullable = false)
    private Long crafting;

    @Column(name = "overall", nullable = false)
    @Setter(AccessLevel.NONE)
    private Long overall;

    public void setOverall(){
        this.overall = attack + defense + magic + cooking + crafting;
    }


}

