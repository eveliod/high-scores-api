package com.highscores.api.util;

public interface CraftingResponse {
    String getNickName();
    int getLevel();
    Long getCrafting();
}
