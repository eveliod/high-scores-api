package com.highscores.api.util;

public interface OverallResponse {
    String getNickName();
    int getLevel();
    Long getOverall();
}

