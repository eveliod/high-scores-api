package com.highscores.api.util;

public interface DefenseResponse {
    String getNickName();
    int getLevel();
    Long getDefense();
}
