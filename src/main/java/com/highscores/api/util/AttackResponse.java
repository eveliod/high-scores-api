package com.highscores.api.util;

public interface AttackResponse {
    String getNickName();
    int getLevel();
    Long getAttack();
}
