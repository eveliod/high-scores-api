package com.highscores.api.util;

public interface MagicResponse {
    String getNickName();
    int getLevel();
    Long getMagic();
}
