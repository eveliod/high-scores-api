package com.highscores.api.util;

public interface CookingResponse {
    String getNickName();
    int getLevel();
    Long getCooking();
}
